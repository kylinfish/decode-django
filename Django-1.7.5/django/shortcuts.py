#!usr/bin/env python
# coding: utf-8

"""
This module collects helper functions and classes that "span" multiple levels
of MVC. In other words, these functions/classes introduce controlled coupling
for convenience's sake.
"""
# KY: 这个模块收集辅助函数和类“span”多层次的MVC. 换句话说, 这些函数/类为方便的缘故引入耦合控制.

from django.template import loader, RequestContext
from django.http import HttpResponse, Http404
from django.http import HttpResponseRedirect, HttpResponsePermanentRedirect
from django.db.models.base import ModelBase
from django.db.models.manager import Manager
from django.db.models.query import QuerySet
from django.core import urlresolvers
from django.utils import six
from django.utils.encoding import force_text
from django.utils.functional import Promise


def render_to_response(*args, **kwargs):
    """
    Returns a HttpResponse whose content is filled with the result of calling
    django.template.loader.render_to_string() with the passed arguments.
    """
    # KY: 返回一个内容充满了的HttpResponse, 它内容是用传入的参数, 调用django.template.loader.render_to_string()的结果.

    httpresponse_kwargs = {'content_type': kwargs.pop('content_type', None)}

    return HttpResponse(loader.render_to_string(*args, **kwargs), **httpresponse_kwargs)


def render(request, *args, **kwargs):
    """
    Returns a HttpResponse whose content is filled with the result of calling
    django.template.loader.render_to_string() with the passed arguments.
    Uses a RequestContext by default.
    """
    # KY: 返回一个内容充满了的HttpResponse, 它内容是用传入的参数, 调用django.template.loader.render_to_string()
    # 的结果. 默认用请求上下文.

    httpresponse_kwargs = {
        'content_type': kwargs.pop('content_type', None),
        'status': kwargs.pop('status', None),
    }

    if 'context_instance' in kwargs:
        context_instance = kwargs.pop('context_instance')
        if kwargs.get('current_app', None):
            raise ValueError('If you provide a context_instance you must '
                             'set its current_app before calling render()')
    else:
        current_app = kwargs.pop('current_app', None)
        context_instance = RequestContext(request, current_app=current_app)

    kwargs['context_instance'] = context_instance

    return HttpResponse(loader.render_to_string(*args, **kwargs),
                        **httpresponse_kwargs)


def redirect(to, *args, **kwargs):
    """
    Returns an HttpResponseRedirect to the appropriate URL for the arguments passed.
    # KY: 用传递的参数, 返回一个到适当的URL的HttpResponseRedirect.

    The arguments could be:
    # KY: 参数可能是:

        * A model: the model's `get_absolute_url()` function will be called.
        # KY: 一个模型: 模型的 'get_absolute_url()' 函数将会被调用.

        * A view name, possibly with arguments: `urlresolvers.reverse()` will be used to reverse-resolve the name.
        # KY: 一个视图名称, 可能与参数: 'urlresolvers.reverse()' 将被用来反向解析这个名字.

        * A URL, which will be used as-is for the redirect location.
        # KY: 一个URL, 将被用于重定向的位置.

    By default issues a temporary redirect; pass permanent=True to issue a permanent redirect.
    # KY: 默认情况下临时重定向; 传值permanent=True永久重定向.
    """
    if kwargs.pop('permanent', False):
        redirect_class = HttpResponsePermanentRedirect
    else:
        redirect_class = HttpResponseRedirect

    return redirect_class(resolve_url(to, *args, **kwargs))


def _get_queryset(klass):
    """
    Returns a QuerySet from a Model, Manager, or QuerySet. Created to make
    get_object_or_404 and get_list_or_404 more DRY.

    # KY: 从Model, Manager, 或者QuerySet, 返回一个QuerySet. 用get_object_or_404和get_list_or_404等创建.

    Raises a ValueError if klass is not a Model, Manager, or QuerySet.
    # KY: 抛出了一个ValueError, 如果klass不是一个Model, Manager, or QuerySet.
    """

    if isinstance(klass, QuerySet):
        return klass
    elif isinstance(klass, Manager):
        manager = klass
    elif isinstance(klass, ModelBase):
        manager = klass._default_manager
    else:
        if isinstance(klass, type):
            klass__name = klass.__name__
        else:
            klass__name = klass.__class__.__name__
        raise ValueError("Object is of type '%s', but must be a Django Model, "
                         "Manager, or QuerySet" % klass__name)
    return manager.all()


def get_object_or_404(klass, *args, **kwargs):
    """
    Uses get() to return an object, or raises a Http404 exception if the object
    does not exist.

    # KY: 使用get()返回一个对象, 或者如果对象不存在时抛出一个Http404异常.

    klass may be a Model, Manager, or QuerySet object. All other passed
    arguments and keyword arguments are used in the get() query.

    # KY: klass可能是一个Model, Manager, 或者QuerySet对象.
    # 所有其他传入参数和关键字参数在get()查询中使用.

    Note: Like with get(), an MultipleObjectsReturned will be raised if more than one
    object is found.

    # KY: 注意: 如同用get(), 如果不止一条对象被发现则抛出一个MultipleObjectsReturned.
    """
    queryset = _get_queryset(klass)
    try:
        return queryset.get(*args, **kwargs)
    except queryset.model.DoesNotExist:
        raise Http404('No %s matches the given query.' % queryset.model._meta.object_name)


def get_list_or_404(klass, *args, **kwargs):
    """
    Uses filter() to return a list of objects, or raise a Http404 exception if
    the list is empty.

    # KY: 使用filter()返回一个对象列表, 或者如果列表是空时抛出一个Http404异常.

    klass may be a Model, Manager, or QuerySet object. All other passed
    arguments and keyword arguments are used in the filter() query.

    # KY: klass可能是一个Model, Manager, 或者QuerySet对象.
    # 所有其他传入参数和关键字参数在filter()查询中使用.
    """
    queryset = _get_queryset(klass)
    obj_list = list(queryset.filter(*args, **kwargs))
    if not obj_list:
        raise Http404('No %s matches the given query.' % queryset.model._meta.object_name)
    return obj_list


def resolve_url(to, *args, **kwargs):
    """
    Return a URL appropriate for the arguments passed.
    # KY: 为传入的参数, 返回一个适合的URL.

    The arguments could be:
    # KY: 参数可能是:

        * A model: the model's `get_absolute_url()` function will be called.
        # KY: 一个模型: 模型的 'get_absolute_url()' 函数将会被调用.

        * A view name, possibly with arguments: `urlresolvers.reverse()` will be used to reverse-resolve the name.
        # KY: 一个视图名称, 可能与参数: 'urlresolvers.reverse()' 将被用来反向解析这个名字.

        * A URL, which will be returned as-is.
        # KY: 一个URL, 将被用于处理返回.

    """
    # If it's a model, use get_absolute_url()
    # KY: 如果它是一个模型, 使用get_absolute_url()
    if hasattr(to, 'get_absolute_url'):
        return to.get_absolute_url()

    if isinstance(to, Promise):
        # Expand the lazy instance, as it can cause issues when it is passed
        # further to some Python functions like urlparse.
        # KY: 扩大懒惰的实例, 因为它会导致问题时通过进一步urlparse等一些Python函数.
        to = force_text(to)

    if isinstance(to, six.string_types):
        # Handle relative URLs
        # KY: 处理相对url.
        if any(to.startswith(path) for path in ('./', '../')):
            return to

    # Next try a reverse URL resolution.
    # KY: 下一个尝试反向地址解析.
    try:
        return urlresolvers.reverse(to, args=args, kwargs=kwargs)
    except urlresolvers.NoReverseMatch:
        # If this is a callable, re-raise.
        # KY: 如果这是一个可调用, re-raise.
        if callable(to):
            raise
        # If this doesn't "feel" like a URL, re-raise.
        # KY: 如果这种“感觉”不像一个URL, re-raise
        if '/' not in to and '.' not in to:
            raise

    # Finally, fall back and assume it's a URL
    # KY: 最后, 回来, 假设这是一个URL
    return to
