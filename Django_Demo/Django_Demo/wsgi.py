#!usr/bin/env python
# coding: utf-8

"""
WSGI config for Django_Demo project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/howto/deployment/wsgi/
"""

# KY:: django 项目的 WSGI 配置文件.

# KY:: 设置环境变量.
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "Django_Demo.settings")

# KY:: 启动Django's WSGI support.

from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()
