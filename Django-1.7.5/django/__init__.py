#!usr/bin/env python
# coding: utf-8

# KY: 版本定义常量.
VERSION = (1, 7, 5, 'final', 0)


def get_version(*args, **kwargs):
    # Don't litter django/__init__.py with all the get_version stuff.
    # Only import if it's actually called.

    # KY: 不要乱动所有get_version附带的这个django/__init__.py文件.
    # 只有import时它才被实际调用.
    from django.utils.version import get_version

    return get_version(*args, **kwargs)


def setup():
    """
    Configure the settings (this happens as a side effect of accessing the
    first setting), configure logging and populate the app registry.
    """
    # KY: 配置设置(作为访问第一个设置的一个副作用发生), 配置日志记录和填充应用程序注册表.

    from django.apps import apps
    from django.conf import settings
    from django.utils.log import configure_logging

    configure_logging(settings.LOGGING_CONFIG, settings.LOGGING)
    apps.populate(settings.INSTALLED_APPS)
