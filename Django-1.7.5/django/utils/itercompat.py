#!usr/bin/env python
# coding: utf-8

"""
Providing iterator functions that are not in all version of Python we support.
Where possible, we try to use the system-native version and only fall back to
these implementations if necessary.
"""

# KY: 不是在我们支持所有Python版本都提供迭代器函数,　在可能的情况
# 下, 我们试着使用本地系统版本, 只有在必要的情况下才会到这些实现.


def is_iterable(x):
    """A implementation independent way of checking for iterables."""
    # KY: 一个实现检查iterable的独立方式.

    try:
        iter(x)
    except TypeError:
        return False
    else:
        return True
