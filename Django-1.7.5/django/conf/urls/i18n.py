#!usr/bin/env python
# coding: utf-8

from django.conf import settings
from django.conf.urls import patterns, url
from django.core.urlresolvers import LocaleRegexURLResolver


def i18n_patterns(prefix, *args):
    """
    Adds the language code prefix to every URL pattern within this
    function. This may only be used in the root URLconf, not in an included
    URLconf.

    # KY: 用这个函数添加语言代码前缀在每个URL模式. 这可能只被用于根URLconf, 不用于包括URLconf.
    """
    pattern_list = patterns(prefix, *args)
    if not settings.USE_I18N:
        return pattern_list
    return [LocaleRegexURLResolver(pattern_list)]


urlpatterns = patterns('', url(r'^setlang/$', 'django.views.i18n.set_language', name='set_language'), )
