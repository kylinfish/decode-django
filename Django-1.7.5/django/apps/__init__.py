# !usr/bin/env python
# coding: utf-8

from .config import AppConfig  # NOQA
from .registry import apps  # NOQA
