#!usr/bin/env python
# coding: utf-8

# Default Django settings. Override these with settings in the module
# pointed-to by the DJANGO_SETTINGS_MODULE environment variable.

# KY: 默认的Django settings. 用环境变量'DJANGO_SETTINGS_MODULE'指定的模块settings重写这些.

# This is defined here as a do-nothing function because we can't import
# django.utils.translation -- that module depends on the settings.

# KY: 在这里定义什么也不做的函数. 因为我们没导入django.utils.translation. 这个翻译模块依赖于此设置.
gettext_noop = lambda s: s

####################
# CORE             #
####################

DEBUG = False
TEMPLATE_DEBUG = False

# Whether the framework should propagate raw exceptions rather than catching
# them. This is useful under some testing situations and should never be used
# on a live site.

# KY: 框架是否应该传播原始异常而不是捕获它们. 在一些测试的情况下这是有用的, 它不应该住活动站使用.
DEBUG_PROPAGATE_EXCEPTIONS = False

# Whether to use the "Etag" header. This saves bandwidth but slows down performance.
# KY: 是否使用 "Etag" header. 这节省带宽, 但降低性能.
USE_ETAGS = False

# People who get code error notifications.
# In the format (('Full Name', 'email@example.com'), ('Full Name', 'anotheremail@example.com'))
# KY: 谁得到代码错误通知. 用元组格式.
ADMINS = ()

# Tuple of IP addresses, as strings, that:
# * See debug comments, when DEBUG is true
# * Receive x-headers

# KY: IP 地址元组, 字符串形式, 这样:
# * 当DEBUG是True时, 看debug内容.
# * 收到x-headers.
INTERNAL_IPS = ()

# Hosts/domain names that are valid for this site.
# "*" matches anything, ".example.com" matches example.com and all subdomains.

# KY: 本网站有效期的主机/域名. '*' 匹配所有, '.'匹配所有子域名.
ALLOWED_HOSTS = []

# Local time zone for this installation. All choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name (although not all
# systems may support all possibilities). When USE_TZ is True, this is
# interpreted as the default user time zone.

# KY: 安装本地时区. 所有的选择都可以在这里找到. 尽管并不是所有的系
# 统可能支持所有的可能性. 当USE_TZ是真的, 这为默认的用户时区使用.
TIME_ZONE = 'America/Chicago'

# If you set this to True, Django will use timezone-aware datetime.
# KY: 如果你设置为True, Django 使用 timezone-aware 时间.
USE_TZ = False

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
# KY: 安装语言代码. 所有的选择都可以在这里找到.
LANGUAGE_CODE = 'en-us'

# Languages we provide translations for, out of the box.
# KY: 我们提供开箱即用的语言翻译.
LANGUAGES = (
    ('af', gettext_noop('Afrikaans')),
    ('ar', gettext_noop('Arabic')),
    ('ast', gettext_noop('Asturian')),
    ('az', gettext_noop('Azerbaijani')),
    ('bg', gettext_noop('Bulgarian')),
    ('be', gettext_noop('Belarusian')),
    ('bn', gettext_noop('Bengali')),
    ('br', gettext_noop('Breton')),
    ('bs', gettext_noop('Bosnian')),
    ('ca', gettext_noop('Catalan')),
    ('cs', gettext_noop('Czech')),
    ('cy', gettext_noop('Welsh')),
    ('da', gettext_noop('Danish')),
    ('de', gettext_noop('German')),
    ('el', gettext_noop('Greek')),
    ('en', gettext_noop('English')),
    ('en-au', gettext_noop('Australian English')),
    ('en-gb', gettext_noop('British English')),
    ('eo', gettext_noop('Esperanto')),
    ('es', gettext_noop('Spanish')),
    ('es-ar', gettext_noop('Argentinian Spanish')),
    ('es-mx', gettext_noop('Mexican Spanish')),
    ('es-ni', gettext_noop('Nicaraguan Spanish')),
    ('es-ve', gettext_noop('Venezuelan Spanish')),
    ('et', gettext_noop('Estonian')),
    ('eu', gettext_noop('Basque')),
    ('fa', gettext_noop('Persian')),
    ('fi', gettext_noop('Finnish')),
    ('fr', gettext_noop('French')),
    ('fy', gettext_noop('Frisian')),
    ('ga', gettext_noop('Irish')),
    ('gl', gettext_noop('Galician')),
    ('he', gettext_noop('Hebrew')),
    ('hi', gettext_noop('Hindi')),
    ('hr', gettext_noop('Croatian')),
    ('hu', gettext_noop('Hungarian')),
    ('ia', gettext_noop('Interlingua')),
    ('id', gettext_noop('Indonesian')),
    ('io', gettext_noop('Ido')),
    ('is', gettext_noop('Icelandic')),
    ('it', gettext_noop('Italian')),
    ('ja', gettext_noop('Japanese')),
    ('ka', gettext_noop('Georgian')),
    ('kk', gettext_noop('Kazakh')),
    ('km', gettext_noop('Khmer')),
    ('kn', gettext_noop('Kannada')),
    ('ko', gettext_noop('Korean')),
    ('lb', gettext_noop('Luxembourgish')),
    ('lt', gettext_noop('Lithuanian')),
    ('lv', gettext_noop('Latvian')),
    ('mk', gettext_noop('Macedonian')),
    ('ml', gettext_noop('Malayalam')),
    ('mn', gettext_noop('Mongolian')),
    ('mr', gettext_noop('Marathi')),
    ('my', gettext_noop('Burmese')),
    ('nb', gettext_noop('Norwegian Bokmal')),
    ('ne', gettext_noop('Nepali')),
    ('nl', gettext_noop('Dutch')),
    ('nn', gettext_noop('Norwegian Nynorsk')),
    ('os', gettext_noop('Ossetic')),
    ('pa', gettext_noop('Punjabi')),
    ('pl', gettext_noop('Polish')),
    ('pt', gettext_noop('Portuguese')),
    ('pt-br', gettext_noop('Brazilian Portuguese')),
    ('ro', gettext_noop('Romanian')),
    ('ru', gettext_noop('Russian')),
    ('sk', gettext_noop('Slovak')),
    ('sl', gettext_noop('Slovenian')),
    ('sq', gettext_noop('Albanian')),
    ('sr', gettext_noop('Serbian')),
    ('sr-latn', gettext_noop('Serbian Latin')),
    ('sv', gettext_noop('Swedish')),
    ('sw', gettext_noop('Swahili')),
    ('ta', gettext_noop('Tamil')),
    ('te', gettext_noop('Telugu')),
    ('th', gettext_noop('Thai')),
    ('tr', gettext_noop('Turkish')),
    ('tt', gettext_noop('Tatar')),
    ('udm', gettext_noop('Udmurt')),
    ('uk', gettext_noop('Ukrainian')),
    ('ur', gettext_noop('Urdu')),
    ('vi', gettext_noop('Vietnamese')),
    ('zh-cn', gettext_noop('Simplified Chinese')),
    ('zh-hans', gettext_noop('Simplified Chinese')),
    ('zh-hant', gettext_noop('Traditional Chinese')),
    ('zh-tw', gettext_noop('Traditional Chinese')),
)

# Languages using BiDi (right-to-left) layout
# KY: 使用BiDi语言(从右到左)布局.
LANGUAGES_BIDI = ("he", "ar", "fa", "ur")

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
# KY: 如果你设置为False, Django将进行一些优化, 以免加载国际化设置.
USE_I18N = True
LOCALE_PATHS = ()

# Settings for language cookie
# KY: 设置语言cookie.
LANGUAGE_COOKIE_NAME = 'django_language'
LANGUAGE_COOKIE_AGE = None
LANGUAGE_COOKIE_DOMAIN = None
LANGUAGE_COOKIE_PATH = '/'

# If you set this to True, Django will format dates, numbers and calendars
# according to user current locale.

# KY: 如果你设置为True, Django将根据用户当前的语言环境, 格式日期, 数字和日历.
USE_L10N = False

# Not-necessarily-technical managers of the site. They get broken link
# notifications and other various emails.

# KY: Not-necessarily-technical站点的管理者. 他们得到失效链接和其他各种电子邮件通知.
MANAGERS = ADMINS

# Default content type and charset to use for all HttpResponse objects, if a
# MIME type isn't manually specified. These are used to construct the
# Content-Type header.

# KY: 所有使用HttpResponse对象的默认内容类型和字符集, 如果一个MIME类型不是手动指定. 这些都是用于构造Content-Type头.
DEFAULT_CONTENT_TYPE = 'text/html'
DEFAULT_CHARSET = 'utf-8'

# Encoding of files read from disk (template and initial SQL files).
# KY: 从磁盘读取(模板和最初的SQL文件)文件的编码.
FILE_CHARSET = 'utf-8'

# Email address that error messages come from.
# KY: 错误信息的发送方Email地址.
SERVER_EMAIL = 'root@localhost'

# Whether to send broken-link emails. Deprecated, must be removed in 1.8.
# KY: 是否发送失效链接的电子邮件. 在1.8已经被删除弃用.
SEND_BROKEN_LINK_EMAILS = False

# Database connection info. If left empty, will default to the dummy backend.
# KY: 数据库连接信息. 如果空, 将默认为假后端.
DATABASES = {}

# Classes used to implement DB routing behavior.
# KY: 用于实现DB路由行为的类.
DATABASE_ROUTERS = []

# The email backend to use. For possible shortcuts see django.core.mail.
# The default is to use the SMTP backend.
# Third-party backends can be specified by providing a Python path
# to a module that defines an EmailBackend class.

# KY: Email的后端使用. 更多快捷看django.core.mail. 默认用SMTP后端.
# KY: 可以通过提供一个Python路径指定一个定义为EmailBackend类作为第三方后端模块.
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

# Host for sending email.
# KY: 发送邮件的主机.
EMAIL_HOST = 'localhost'

# Port for sending email.
# KY: 发送邮件的端口.
EMAIL_PORT = 25

# Optional SMTP authentication information for EMAIL_HOST.
# KY: 为 EMAIL_HOST 的 SMTP 身份验证信息可选项.
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_USE_TLS = False
EMAIL_USE_SSL = False

# List of strings representing installed apps.
# KY: 代表已安装应用程序字符串列表.
INSTALLED_APPS = ()

# List of locations of the template source files, in search order.
# KY: 按搜索顺序的模板源文件的位置列表.
TEMPLATE_DIRS = ()

# List of callables that know how to import templates from various sources.
# See the comments in django/core/template/loader.py for interface
# documentation.

# KY: 知道如何从各种来源导入模板的调用列表.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    # 'django.template.loaders.eggs.Loader',
)

# List of processors used by RequestContext to populate the context.
# Each one should be a callable that takes the request object as its
# only parameter and returns a dictionary to add to the context.

# KY: 用于RequestContext填充上下文的处理器列表. 每个都是一个
# 可调用的请求对象作为其唯一的参数, 并返回一个添加上下文字典.
TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.tz',
    # 'django.core.context_processors.request',
    'django.contrib.messages.context_processors.messages',
)

# Output to use in template system for invalid (e.g. misspelled) variables.
# KY: 模板系统中为无效(如拼写错误)变量输出使用.
TEMPLATE_STRING_IF_INVALID = ''

# Default email address to use for various automated correspondence from
# the site managers.
# KY: 默认邮件地址用于网站管理者各种自动化通信.
DEFAULT_FROM_EMAIL = 'webmaster@localhost'

# Subject-line prefix for email messages send with django.core.mail.mail_admins
# or ...mail_managers.  Make sure to include the trailing space.

# KY: 用django.core.mail.mail_admins 或mail_managers发送Email消息的主题前缀. 确定它包含一个空格结尾.
EMAIL_SUBJECT_PREFIX = '[Django] '

# Whether to append trailing slashes to URLs.
# KY: 是否斜杠附加到url.
APPEND_SLASH = True

# Whether to prepend the "www." subdomain to URLs that don't have it.
# KY: 如果没提供, 是否"www."附加到url.
PREPEND_WWW = False

# Override the server-derived value of SCRIPT_NAME
# KY: 重写的 SCRIPT_NAME server-derived 值.
FORCE_SCRIPT_NAME = None

# List of compiled regular expression objects representing User-Agent strings
# that are not allowed to visit any page, systemwide. Use this for bad
# robots/crawlers. Here are a few examples:

# KY: 表示用户代理字符串已编译正则表达式对象列表, 使用这个不允许robots/crawlers
# 访问全系统的任何页面. 这里有一些例子:

# import re
# DISALLOWED_USER_AGENTS = (
# re.compile(r'^NaverBot.*'),
# re.compile(r'^EmailSiphon.*'),
# re.compile(r'^SiteSucker.*'),
# re.compile(r'^sohu-search')
# )

DISALLOWED_USER_AGENTS = ()
ABSOLUTE_URL_OVERRIDES = {}

# Tuple of strings representing allowed prefixes for the {% ssi %} tag.
# Example: ('/home/html', '/var/www')
# KY: 字符串的元组代表允许前缀{% ssi %}标记.
ALLOWED_INCLUDE_ROOTS = ()

# If this is an admin settings module, this should be a list of settings
# modules (in the format 'foo.bar.baz') for which this admin is an admin.

# KY: 如果这是管理员设置模块, 这应该是一个设置模块的列表(格式 'foo.bar.baz').
ADMIN_FOR = ()

# List of compiled regular expression objects representing URLs that need not
# be reported by BrokenLinkEmailsMiddleware. Here are a few examples:

# KY: 代表的url不需要BrokenLinkEmailsMiddleware报道的已编译正则表达式对象列表. 这里有一些例子.

# import re
# IGNORABLE_404_URLS = (
# re.compile(r'^/apple-touch-icon.*\.png$'),
# re.compile(r'^/favicon.ico$),
# re.compile(r'^/robots.txt$),
# re.compile(r'^/phpmyadmin/),
# re.compile(r'\.(cgi|php|pl)$'),
# )
IGNORABLE_404_URLS = ()

# A secret key for this particular Django installation. Used in secret-key
# hashing algorithms. Set this in your settings, or Django will complain loudly.
# KY: 这个特殊的Django安装密钥. 用于散列算法. 这个在你的设置设置, 否则Django会强列抱怨.
SECRET_KEY = ''

# Default file storage mechanism that holds media.
# KY: 默认的媒体文件存储机制.
DEFAULT_FILE_STORAGE = 'django.core.files.storage.FileSystemStorage'

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
# KY: 被用上存储用户上传文件的绝对文件系统目录路径.
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT.
# Examples: "http://example.com/media/", "http://media.example.com/"
# KY: URL处理媒体从媒体根.
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Example: "/var/www/example.com/static/"
# KY: 收集静态文件的绝对路径目录.
STATIC_ROOT = None

# URL that handles the static files served from STATIC_ROOT.
# Example: "http://example.com/static/", "http://static.example.com/"
# KY: 来自STATIC_ROOT的处理静态文件服务的URL.
STATIC_URL = None

# List of upload handler classes to be applied in order.
# KY: 上传列表处理程序类应用.
FILE_UPLOAD_HANDLERS = (
    'django.core.files.uploadhandler.MemoryFileUploadHandler',
    'django.core.files.uploadhandler.TemporaryFileUploadHandler',
)

# Maximum size, in bytes, of a request before it will be streamed to the
# file system instead of into memory.

# KY: 以字节为单位, 一个请求的最大尺寸, 它到文件系统而不是到内存中之前.
FILE_UPLOAD_MAX_MEMORY_SIZE = 2621440  # i.e. 2.5 MB

# Directory in which upload streamed files will be temporarily saved. A value of
# `None` will make Django use the operating system's default temporary directory
# (i.e. "/tmp" on *nix systems).

# KY: 上传流文件将暂时保存目录. 值为'None'将Django使用操作系统的默认临时目录.
FILE_UPLOAD_TEMP_DIR = None

# The numeric mode to set newly-uploaded files to. The value should be a mode
# you'd pass directly to os.chmod; see http://docs.python.org/lib/os-file-dir.html

# KY: 设置上传文件数值模式, 值应该是一个你直接上传的os.chmod模式.
FILE_UPLOAD_PERMISSIONS = None

# The numeric mode to assign to newly-created directories, when uploading files.
# The value should be a mode as you'd pass to os.chmod;
# see http://docs.python.org/lib/os-file-dir.html

# KY: 当上传文件, 分配新目录数值模式. 值应该是你传入的一个os.chmod模式.
FILE_UPLOAD_DIRECTORY_PERMISSIONS = None

# Python module path where user will place custom format definition.
# The directory where this setting is pointing should contain subdirectories
# named as the locales, containing a formats.py file
# (i.e. "myproject.locale" for myproject/locale/en/formats.py etc. use)

# KY: 用户定义自己格式的Python模块路径, 这个设置的指向的目录应该包
# 含命名为 'locales' 的子目录, 包含一个formats.py文件.
FORMAT_MODULE_PATH = None

# Default formatting for date objects. See all available format strings here:
# http://docs.djangoproject.com/en/dev/ref/templates/builtins/#date

# KY: 默认格式化date对象, 在这里看到所有可用的格式字符串:
DATE_FORMAT = 'N j, Y'

# Default formatting for datetime objects. See all available format strings here:
# http://docs.djangoproject.com/en/dev/ref/templates/builtins/#date
# KY: 默认格式化datetime对象, 在这里看到所有可用的格式字符串:
DATETIME_FORMAT = 'N j, Y, P'

# Default formatting for time objects. See all available format strings here:
# http://docs.djangoproject.com/en/dev/ref/templates/builtins/#date

# KY: 默认格式化time对象, 在这里看到所有可用的格式字符串:
TIME_FORMAT = 'P'

# Default formatting for date objects when only the year and month are relevant.
# See all available format strings here:
# http://docs.djangoproject.com/en/dev/ref/templates/builtins/#date

# KY: 只有年和月相关的默认格式化date对象, 在这里看到所有可用的格式字符串:
YEAR_MONTH_FORMAT = 'F Y'

# Default formatting for date objects when only the month and day are relevant.
# See all available format strings here:
# http://docs.djangoproject.com/en/dev/ref/templates/builtins/#date

# KY: 只有月和天相关的默认格式化日期对象, 在这里看到所有可用的格式字符串:
MONTH_DAY_FORMAT = 'F j'

# Default short formatting for date objects. See all available format strings here:
# http://docs.djangoproject.com/en/dev/ref/templates/builtins/#date

# KY:默认短格式化date对象, 在这里看到所有可用的格式字符串:
SHORT_DATE_FORMAT = 'm/d/Y'

# Default short formatting for datetime objects.
# See all available format strings here:
# http://docs.djangoproject.com/en/dev/ref/templates/builtins/#date

# KY:默认短格式化datetime对象, 在这里看到所有可用的格式字符串:
SHORT_DATETIME_FORMAT = 'm/d/Y P'

# Default formats to be used when parsing dates from input boxes, in order
# See all available format string here:
# http://docs.python.org/library/datetime.html#strftime-behavior
# * Note that these format strings are different from the ones to display dates.

# KY: 使用从输入框时解析日期的默认格式, 为了在这里看到所有可用的格式字符串:
# 注意, 这些格式字符串的显示日期不同.
DATE_INPUT_FORMATS = (
    '%Y-%m-%d', '%m/%d/%Y', '%m/%d/%y',  # '2006-10-25', '10/25/2006', '10/25/06'
    '%b %d %Y', '%b %d, %Y',  # 'Oct 25 2006', 'Oct 25, 2006'
    '%d %b %Y', '%d %b, %Y',  # '25 Oct 2006', '25 Oct, 2006'
    '%B %d %Y', '%B %d, %Y',  # 'October 25 2006', 'October 25, 2006'
    '%d %B %Y', '%d %B, %Y',  # '25 October 2006', '25 October, 2006'
)

# Default formats to be used when parsing times from input boxes, in order
# See all available format string here:
# http://docs.python.org/library/datetime.html#strftime-behavior
# * Note that these format strings are different from the ones to display dates.

# KY: 使用从输入框时解析时间的默认格式, 为了在这里看到所有可用的格式字符串:
# 注意, 这些格式字符串的显示日期不同.
TIME_INPUT_FORMATS = (
    '%H:%M:%S',  # '14:30:59'
    '%H:%M:%S.%f',  # '14:30:59.000200'
    '%H:%M',  # '14:30'
)

# Default formats to be used when parsing dates and times from input boxes, in order
# See all available format string here:
# http://docs.python.org/library/datetime.html#strftime-behavior
# * Note that these format strings are different from the ones to display dates.

# KY: 使用从输入框时解析日期和时间的默认格式, 为了在这里看到所有可用的格式字符串:
# 注意, 这些格式字符串的显示日期不同.
DATETIME_INPUT_FORMATS = (
    '%Y-%m-%d %H:%M:%S',  # '2006-10-25 14:30:59'
    '%Y-%m-%d %H:%M:%S.%f',  # '2006-10-25 14:30:59.000200'
    '%Y-%m-%d %H:%M',  # '2006-10-25 14:30'
    '%Y-%m-%d',  # '2006-10-25'
    '%m/%d/%Y %H:%M:%S',  # '10/25/2006 14:30:59'
    '%m/%d/%Y %H:%M:%S.%f',  # '10/25/2006 14:30:59.000200'
    '%m/%d/%Y %H:%M',  # '10/25/2006 14:30'
    '%m/%d/%Y',  # '10/25/2006'
    '%m/%d/%y %H:%M:%S',  # '10/25/06 14:30:59'
    '%m/%d/%y %H:%M:%S.%f',  # '10/25/06 14:30:59.000200'
    '%m/%d/%y %H:%M',  # '10/25/06 14:30'
    '%m/%d/%y',  # '10/25/06'
)

# First day of week, to be used on calendars
# 0 means Sunday, 1 means Monday...
# KY: 星期的第一天, 被使用在日历上, 0表示星期天, 1意味着星期一.
FIRST_DAY_OF_WEEK = 0

# Decimal separator symbol.
# KY: 十进制分隔符.
DECIMAL_SEPARATOR = '.'

# Boolean that sets whether to add thousand separator when formatting numbers.
# KY: 当格式化数字时, 是否添加千位分隔符.
USE_THOUSAND_SEPARATOR = False

# Number of digits that will be together, when splitting them by
# THOUSAND_SEPARATOR. 0 means no grouping, 3 means splitting by thousands...
# KY: 当大数字时, 用THOUSAND_SEPARATOR分裂他们. 0意味着没有分组, 3意味着按千分位分隔.
NUMBER_GROUPING = 0

# Thousand separator symbol
# KY: 千位分隔符
THOUSAND_SEPARATOR = ','

# Do you want to manage transactions manually?
# Hint: you really don't!
# KY: 你想要手动管理事务? 你真的不需要.
TRANSACTIONS_MANAGED = False

# The tablespaces to use for each model when not specified otherwise.
# KY: 没有指定时每个模型的使用表空间.
DEFAULT_TABLESPACE = ''
DEFAULT_INDEX_TABLESPACE = ''

# Default X-Frame-Options header value.
# KY: 默认的X-Frame-Options头值.
X_FRAME_OPTIONS = 'SAMEORIGIN'

USE_X_FORWARDED_HOST = False

# The Python dotted path to the WSGI application that Django's internal servers
# (runserver, runfcgi) will use. If `None`, the return value of
# 'django.core.wsgi.get_wsgi_application' is used, thus preserving the same
# behavior as previous versions of Django. Otherwise this should point to an
# actual WSGI application object.

# KY: Django应用程序内部服务器(runserver runfcgi)将使用的WSGI Python点缀路径. 如果'None'
# 返回 'django.core.wsgi.get_wsgi_application'的值. 从而保护Django的与以前的版本相同的行
# 为. 否则这指向一个实际WSGI应用程序对象.
WSGI_APPLICATION = None

# If your Django app is behind a proxy that sets a header to specify secure
# connections, AND that proxy ensures that user-submitted headers with the
# same name are ignored (so that people can't spoof it), set this value to
# a tuple of (header_name, header_value). For any requests that come in with
# that header/value, request.is_secure() will return True.
# WARNING! Only set this if you fully understand what you're doing. Otherwise,
# you may be opening yourself up to a security risk.

# KY: 如果您的Django应用程序一个代理的背后, 设置header为特殊的安全连接, 并且代理确保用户提交的
# 名称相同的标头被忽略(所以人们不能恶搞), 将这个值设置为一个(header_name, header_value)元组.
# 任何header/value的请求, request.is_secure()都将返回True.

# KY: 警告! 如果你完全理解你在做什么, 才能做这个. 否则, 你可能会打开自己的安全风险.
SECURE_PROXY_SSL_HEADER = None

##############
# MIDDLEWARE #
##############

# List of middleware classes to use.  Order is important; in the request phase,
# this middleware classes will be applied in the order given, and in the
# response phase the middleware will be applied in reverse order.

# KY: 使用中间件类的列表. 顺序很重要; 在请求阶段, 这个中间件类将被应用的顺序, 并在响应阶段中间件将应用于相反的顺序.
MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
)

############
# SESSIONS #
############

# Cache to store session data if using the cache session backend.
# KY: 如果用session后端缓存, 存储session数据的缓存.
SESSION_CACHE_ALIAS = 'default'

# Cookie name. This can be whatever you want.
# KY: 你想要的那个cookie名称.
SESSION_COOKIE_NAME = 'sessionid'

# Age of cookie, in seconds (default: 2 weeks).
# KY: cookie生命期, 用秒默认2周.
SESSION_COOKIE_AGE = 60 * 60 * 24 * 7 * 2

# A string like ".example.com", or None for standard domain cookie.
# KY: 一个像这样 ".example.com" 的字符串. 为None表示标准域cookie.
SESSION_COOKIE_DOMAIN = None

# Whether the session cookie should be secure (https:// only).
# KY: session cookie是否安全.
SESSION_COOKIE_SECURE = False

# The path of the session cookie.
# KY: session cookie的路径.
SESSION_COOKIE_PATH = '/'

# Whether to use the non-RFC standard httpOnly flag (IE, FF3+, others)
# KY: 是否用HttpOnly.
SESSION_COOKIE_HTTPONLY = True

# Whether to save the session data on every request.
# KY: 是否每次请求时都保存session.
SESSION_SAVE_EVERY_REQUEST = False

# Whether a user's session cookie expires when the Web browser is closed.
# KY: 当浏览器关闭, 用户的session cookie 是否过期.
SESSION_EXPIRE_AT_BROWSER_CLOSE = False

# The module to store session data
# KY: 存储session数据的模块.
SESSION_ENGINE = 'django.contrib.sessions.backends.db'

# Directory to store session files if using the file session module.
# If None, the backend will use a sensible default.
# KY: 如果用文件session模块, 存储session文件的目录. 如果None后端将用默认.
SESSION_FILE_PATH = None

# class to serialize session data
# KY: 序列化session数据的类.
SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'

#########
# CACHE #
#########

# The cache backends to use.
# KY: 后端使用缓存.
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    }
}
CACHE_MIDDLEWARE_KEY_PREFIX = ''
CACHE_MIDDLEWARE_SECONDS = 600
CACHE_MIDDLEWARE_ALIAS = 'default'

####################
# COMMENTS         #
####################

COMMENTS_ALLOW_PROFANITIES = False

# The profanities that will trigger a validation error in
# CommentDetailsForm.clean_comment. All of these should be in lowercase.

# KY: 在CommentDetailsForm.clean_comment, 脏词触发一个验证错误. 所有这些应该是小写.
PROFANITIES_LIST = ()

##################
# AUTHENTICATION #
##################

AUTH_USER_MODEL = 'auth.User'
AUTHENTICATION_BACKENDS = ('django.contrib.auth.backends.ModelBackend',)

LOGIN_URL = '/accounts/login/'
LOGOUT_URL = '/accounts/logout/'
LOGIN_REDIRECT_URL = '/accounts/profile/'

# The number of days a password reset link is valid for
# KY: 一个密码重置链接是有效的天数.
PASSWORD_RESET_TIMEOUT_DAYS = 3

# the first hasher in this list is the preferred algorithm.  any
# password using different algorithms will be converted automatically
# upon login.

# KY: 这个列表第一哈希器是首选算法. 任何使用不同的算法的密码在登录时将自动转换.
PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
    'django.contrib.auth.hashers.SHA1PasswordHasher',
    'django.contrib.auth.hashers.MD5PasswordHasher',
    'django.contrib.auth.hashers.UnsaltedSHA1PasswordHasher',
    'django.contrib.auth.hashers.UnsaltedMD5PasswordHasher',
    'django.contrib.auth.hashers.CryptPasswordHasher',
)

###########
# SIGNING #
###########

SIGNING_BACKEND = 'django.core.signing.TimestampSigner'

########
# CSRF #
########

# Dotted path to callable to be used as view when a request is rejected by the CSRF middleware.
# KY: 当一个被使用视图的请求被CSRF中间件拒绝时, 可调用虚线路径.
CSRF_FAILURE_VIEW = 'django.views.csrf.csrf_failure'

# Settings for CSRF cookie.
# KY: CSRF cookie 设置.
CSRF_COOKIE_NAME = 'csrftoken'
CSRF_COOKIE_AGE = 60 * 60 * 24 * 7 * 52
CSRF_COOKIE_DOMAIN = None
CSRF_COOKIE_PATH = '/'
CSRF_COOKIE_SECURE = False
CSRF_COOKIE_HTTPONLY = False

############
# MESSAGES #
############

# Class to use as messages backend.
# KY: 消息后端使用类
MESSAGE_STORAGE = 'django.contrib.messages.storage.fallback.FallbackStorage'

# Default values of MESSAGE_LEVEL and MESSAGE_TAGS are defined within
# django.contrib.messages to avoid imports in this settings file.

# KY: MESSAGE_LEVEL 和 MESSAGE_TAGS 的默认值在django.contrib.messages内定义, 以避免在这设置文件导入.

###########
# LOGGING #
###########

# The callable to use to configure logging.
# KY: 可调用的配置日志记录使用.
LOGGING_CONFIG = 'logging.config.dictConfig'

# Custom logging configuration.
# KY: 自定义日志配置.
LOGGING = {}

# Default exception reporter filter class used in case none has been
# specifically assigned to the HttpRequest instance.
# KY: 使用的默认异常报告过滤器, 以防没有专门分配给HttpRequest实例.
DEFAULT_EXCEPTION_REPORTER_FILTER = 'django.views.debug.SafeExceptionReporterFilter'

###########
# TESTING #
###########

# The name of the class to use to run the test suite.
# KY: 用来运行测试套件的类名称.
TEST_RUNNER = 'django.test.runner.DiscoverRunner'

# Apps that don't need to be serialized at test database creation time
# (only apps with migrations are to start with)
# KY: 在创建测试数据库时应用程序不需要序列化. 仅仅迁移应用程序开始时.
TEST_NON_SERIALIZED_APPS = []

############
# FIXTURES #
############

# The list of directories to search for fixtures.
# KY: 搜索设备的目录列表.
FIXTURE_DIRS = ()

###############
# STATICFILES #
###############

# A list of locations of additional static files.
# KY: 额外的静态文件的位置列表.
STATICFILES_DIRS = ()

# The default file storage backend used during the build process.
# KY: 在构建过程中使用的默认文件存储后端.
STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.StaticFilesStorage'

# List of finder classes that know how to find static files in various locations.
# KY: 知道如何在不同地点找到静态文件的finder列表类.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # 'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

##############
# MIGRATIONS #
##############

# Migration module overrides for apps, by app label.
# KY: 迁移模块重写应用程序, 应用程序标签.
MIGRATION_MODULES = {}

#################
# SYSTEM CHECKS #
#################

# List of all issues generated by system checks that should be silenced. Light
# issues like warnings, infos or debugs will not generate a message. Silencing
# serious issues like errors and criticals does not result in hiding the
# message, but Django will not stop you from e.g. running server.

# KY: 由静默系统检查生成的所有问题列表. 诸如警告, 信息或调试小问题不会生成一条消息.
# 像errors和criticals等严重问题默认不会导致隐藏信息, 但Django不会阻止你如运行服务器.
SILENCED_SYSTEM_CHECKS = []
