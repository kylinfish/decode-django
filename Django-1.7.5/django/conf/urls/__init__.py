#!usr/bin/env python
# coding: utf-8

from importlib import import_module

from django.core.urlresolvers import (RegexURLPattern,
                                      RegexURLResolver, LocaleRegexURLResolver)
from django.core.exceptions import ImproperlyConfigured
from django.utils import six


__all__ = ['handler400', 'handler403', 'handler404', 'handler500', 'include', 'patterns', 'url']

handler400 = 'django.views.defaults.bad_request'
handler403 = 'django.views.defaults.permission_denied'
handler404 = 'django.views.defaults.page_not_found'
handler500 = 'django.views.defaults.server_error'


def include(arg, namespace=None, app_name=None):
    # KY: 使用于在根URLS引入APP的URLS.

    if isinstance(arg, tuple):
        # callable returning a namespace hint
        # KY: 可返回一个命名空间线索.
        if namespace:
            raise ImproperlyConfigured('Cannot override the namespace for a dynamic module that provides a namespace')
        urlconf_module, app_name, namespace = arg
    else:
        # No namespace hint - use manually provided namespace
        # KY: 没有命名空间线索 — 使用手动提供名称空间.
        urlconf_module = arg

    if isinstance(urlconf_module, six.string_types):
        urlconf_module = import_module(urlconf_module)

    _patterns = getattr(urlconf_module, 'urlpatterns', urlconf_module)

    # Make sure we can iterate through the patterns (without this, some testcases will break).
    # KY: 确保我们可以遍历模式(没有这个, 有些用例将打破).
    if isinstance(_patterns, (list, tuple)):
        for url_pattern in _patterns:
            # Test if the LocaleRegexURLResolver is used within the include;
            # this should throw an error since this is not allowed!

            # KY: 测试LocaleRegexURLResolver是否在include内被使用.
            # 这应该抛出一个错误, 因为这是不被允许的.
            if isinstance(url_pattern, LocaleRegexURLResolver):
                raise ImproperlyConfigured(
                    'Using i18n_patterns in an included URLconf is not allowed.')

    return urlconf_module, app_name, namespace


def patterns(prefix, *args):
    # KY: 获得URL解析后的集合.

    pattern_list = []
    for t in args:
        if isinstance(t, (list, tuple)):
            t = url(prefix=prefix, *t)
        elif isinstance(t, RegexURLPattern):
            t.add_prefix(prefix)
        pattern_list.append(t)
    return pattern_list


def url(regex, view, kwargs=None, name=None, prefix=''):
    # KY:正则URL的解析, 定位到指定view.

    if isinstance(view, (list, tuple)):
        # For include(...) processing.
        # KY: 处理正则与app下urls的关联. 为处理include().
        urlconf_module, app_name, namespace = view
        return RegexURLResolver(regex, urlconf_module, kwargs, app_name=app_name, namespace=namespace)
    else:
        # KY: 处理正则与URL的关联.
        if isinstance(view, six.string_types):
            if not view:
                raise ImproperlyConfigured('Empty URL pattern view name not permitted (for pattern %r)' % regex)
            if prefix:
                view = '.'.join([prefix, view])

        return RegexURLPattern(regex, view, kwargs, name)
