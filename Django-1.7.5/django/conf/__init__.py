#!usr/bin/env python
# coding: utf-8

"""
Settings and configuration for Django.

Values will be read from the module specified by the DJANGO_SETTINGS_MODULE environment
variable, and then from django.conf.global_settings; see the global settings file for
a list of all possible variables.
"""
# KY: 为Django设置和配置. 值将从指定的DJANGO环境变量设置模块读取.
# 来自于django.conf.global_settings, 一个所有可用变量列表, 看global settings文件.

import importlib
import os
import time  # Needed for Windows

from django.conf import global_settings
from django.core.exceptions import ImproperlyConfigured
from django.utils.functional import LazyObject, empty
from django.utils import six

# 默认的环境变量Key设置
ENVIRONMENT_VARIABLE = "DJANGO_SETTINGS_MODULE"


class LazySettings(LazyObject):
    """
    A lazy proxy for either global Django settings or a custom settings object.
    The user can manually configure settings prior to using them. Otherwise,
    Django uses the settings module pointed to by DJANGO_SETTINGS_MODULE.
    """
    # KY: 一个懒惰的代理为全局Django设置或一个自定义设置对象.
    # 用户可以使用它们之前手动配置设置, 否则, Django使用由'DJANGO_SETTINGS_MODULE'指定的设置模块

    def _setup(self, name=None):
        """
        Load the settings module pointed to by the environment variable. This
        is used the first time we need any settings at all, if the user has not
        previously configured the settings manually.
        """
        # KY: 装载由'DJANGO_SETTINGS_MODULE'指定的设置模块. 这是我们第一次使用
        # 需要任何设置, 如果用户没有以前手动配置设置.

        # KY: 转载默认环境变量值.
        settings_module = os.environ.get(ENVIRONMENT_VARIABLE)
        if not settings_module:
            desc = ("setting %s" % name) if name else "settings"
            raise ImproperlyConfigured(
                "Requested %s, but settings are not configured. "
                "You must either define the environment variable %s "
                "or call settings.configure() before accessing settings."
                % (desc, ENVIRONMENT_VARIABLE))

        self._wrapped = Settings(settings_module)

    def __getattr__(self, name):
        if self._wrapped is empty:
            self._setup(name)
        return getattr(self._wrapped, name)

    def configure(self, default_settings=global_settings, **options):
        """
        Called to manually configure the settings. The 'default_settings'
        parameter sets where to retrieve any unspecified values from (its
        argument must support attribute access (__getattr__)).
        """
        # KY: 手工配置设置调用. 'default_settings'的参数设置, 以
        # 检索任何未指定的值(它的参数必须支持属性访问(__getattr__)).

        if self._wrapped is not empty:
            raise RuntimeError('Settings already configured.')

        holder = UserSettingsHolder(default_settings)
        for name, value in options.items():
            setattr(holder, name, value)

        self._wrapped = holder

    @property
    def configured(self):
        """
        Returns True if the settings have already been configured.
        """
        # KY: 如果settings已经配置则返回True.
        return self._wrapped is not empty


class BaseSettings(object):
    """
    Common logic for settings whether set by a module or by the user.
    """
    # KY:　settings 是否被一个模块或用户设置的通用逻辑.

    def __setattr__(self, name, value):
        if name in ("MEDIA_URL", "STATIC_URL") and value and not value.endswith('/'):
            raise ImproperlyConfigured("If set, %s must end with a slash" % name)
        elif name == "ALLOWED_INCLUDE_ROOTS" and isinstance(value, six.string_types):
            raise ValueError("The ALLOWED_INCLUDE_ROOTS setting must be set "
                             "to a tuple, not a string.")

        # KY: 增加了三个特殊设置项的数据格式校正, 然后才是调用基类该方法.
        object.__setattr__(self, name, value)


class Settings(BaseSettings):
    def __init__(self, settings_module):
        # update this dict from global settings (but only for ALL_CAPS settings)

        # KY: 从全局设置里更新这个字典, 但是仅仅为全部大写的设置.
        # 此过程之后, 该类的一个实例拥有global_settings的一份副本.
        for setting in dir(global_settings):
            if setting.isupper():
                setattr(self, setting, getattr(global_settings, setting))

        # store the settings module in case someone later cares
        # KY: 存储设置模块, 以防有人干预. 该传参设置模块为项目中实际settings.
        self.SETTINGS_MODULE = settings_module

        # KY: 导入该传参项目中实际settings.
        try:
            mod = importlib.import_module(self.SETTINGS_MODULE)
        except ImportError as e:
            raise ImportError(
                "Could not import settings '%s' (Is it on sys.path? Is there an import error in the settings file?): %s"
                % (self.SETTINGS_MODULE, e)
            )

        tuple_settings = ("INSTALLED_APPS", "TEMPLATE_DIRS")

        # KY: 此变量存储项目中实际settings中Key集合.
        self._explicit_settings = set()

        # KY: 该类的实例已经拥有global_settings的一份副本.
        # 然后从项目中实际settings, 更新该份副本的值.
        for setting in dir(mod):
            if setting.isupper():
                setting_value = getattr(mod, setting)

                # KY: 验证特殊两项的数据类型.
                if (setting in tuple_settings and
                        isinstance(setting_value, six.string_types)):
                    raise ImproperlyConfigured("The %s setting must be a tuple. "
                                               "Please fix your settings." % setting)
                setattr(self, setting, setting_value)
                self._explicit_settings.add(setting)

        # 检查SECRET_KEY必需项
        if not self.SECRET_KEY:
            raise ImproperlyConfigured("The SECRET_KEY setting must not be empty.")

        if hasattr(time, 'tzset') and self.TIME_ZONE:
            # When we can, attempt to validate the timezone. If we can't find
            # this file, no check happens and it's harmless.

            # KY: 当我们可以尝试验证时区. 如果我们找不到这个文件, 没有检查发生和它是无害的.
            zoneinfo_root = '/usr/share/zoneinfo'
            if (os.path.exists(zoneinfo_root) and not
            os.path.exists(os.path.join(zoneinfo_root, *(self.TIME_ZONE.split('/'))))):
                raise ValueError("Incorrect timezone setting: %s" % self.TIME_ZONE)

            # Move the time zone info into os.environ. See ticket #2315 for why
            # we don't do this unconditionally (breaks Windows).

            # KY: 移动时区信息到os.environ. 看ticket #2315 为什么我们不无条件做这个(垃圾破烂windows).
            # https://code.djangoproject.com/ticket/2315
            os.environ['TZ'] = self.TIME_ZONE
            time.tzset()

    def is_overridden(self, setting):
        # KY: 判断某个项是否在项目中重写.
        return setting in self._explicit_settings


class UserSettingsHolder(BaseSettings):
    """
    Holder for user configured settings.
    """
    # KY: 用户配置设置.

    # SETTINGS_MODULE doesn't make much sense in the manually configured (standalone) case.
    # KY: SETTINGS_MODULE 在手动配置(独立)情况下没有多大意义.

    SETTINGS_MODULE = None

    def __init__(self, default_settings):
        """
        Requests for configuration variables not in this class are satisfied
        from the module specified in default_settings (if possible).
        """
        # KY: 要求不是在这个类的配置变量要满足在默认设置模块中的指定(如果可能的话).

        self.__dict__['_deleted'] = set()
        self.default_settings = default_settings

    def __getattr__(self, name):
        if name in self._deleted:
            raise AttributeError
        return getattr(self.default_settings, name)

    def __setattr__(self, name, value):
        self._deleted.discard(name)
        super(UserSettingsHolder, self).__setattr__(name, value)

    def __delattr__(self, name):
        self._deleted.add(name)
        if hasattr(self, name):
            super(UserSettingsHolder, self).__delattr__(name)

    def __dir__(self):
        return list(self.__dict__) + dir(self.default_settings)

    def is_overridden(self, setting):
        deleted = (setting in self._deleted)
        set_locally = (setting in self.__dict__)
        set_on_default = getattr(self.default_settings, 'is_overridden', lambda s: False)(setting)
        return (deleted or set_locally or set_on_default)

# 缺省时默认设置
settings = LazySettings()
