Changelog
=========

15-Mar-2015
-----------
* Complete the following directory file code comment: apps, bin, conf, dispatch, core/checks.
* Complete the root file code comment: ['__init__.py', 'shortcuts.py']


02-Mar-2015
-----------

* Configure development environment.
* Add develop environment document.


26-Feb-2015
-----------

* First initialization.
* Add Django-1.7.5 Files.
