# !usr/bin/env python
# coding: utf-8

from django.core import management

if __name__ == "__main__":
    management.execute_from_command_line()

    # KY: 如同项目根目录下manage.py, 与它相比缺少了环境变量 "DJANGO_SETTINGS_MODULE" 的设置.
    # 函数 execute_from_command_line 的参数构建放置最里层, 非参的形式.
