#!usr/bin/env python
# coding: utf-8

from __future__ import unicode_literals
from django.apps import apps
from .. import Warning, register, Tags


@register(Tags.compatibility)
def check_1_6_compatibility(**kwargs):
    # KY: 检查代码对1.6的兼容性.

    errors = []
    errors.extend(_check_test_runner(**kwargs))
    errors.extend(_check_boolean_field_default_value(**kwargs))
    return errors


def _check_test_runner(app_configs=None, **kwargs):
    """
    Checks if the user has *not* overridden the ``TEST_RUNNER`` setting &
    warns them about the default behavior changes.

    # KY: 检查测试器. 检查用户是否已经重写 'TEST_RUNNER' 设置, 警告他们关于默认的变化.

    If the user has overridden that setting, we presume they know what they're
    doing & avoid generating a message.

    # KY: 如果用户已经重写了设置, 我们认为他们知道他们在做什么和避免生成一条消息.
    """

    from django.conf import settings

    # We need to establish if this is a project defined on the 1.5 project template,
    # because if the project was generated on the 1.6 template, it will have be been
    # developed with the new TEST_RUNNER behavior in mind.

    # KY: 我们需要确定这是否为一个定义在1.5项目模板的项目, 因为如果是基于1.6模板生成, 这将是开发新的测试运行器行为.

    # There's no canonical way to do this; so we leverage off the fact that 1.6
    # also introduced a new project template, removing a bunch of settings from the
    # default that won't be in common usage.

    # KY: 做这些没有规范的方式, 所以我们利用1.6, 还引入了一个新项目模板, 移除一群不通用使用的默认设置.

    # We make this determination on a balance of probabilities. Each of these factors
    # contributes a weight; if enough of them trigger, we've got a likely 1.6 project.

    # KY: 我们制造一个可能性平衡的决心. 所有这些因素的贡献权重; 如果足够触发, 我们得到是类似1.6项目.
    weight = 0

    # If TEST_RUNNER is explicitly set, it's all a moot point - if it's been explicitly set,
    # the user has opted into a specific set of behaviors, which won't change as the
    # default changes.

    # KY: 如果 TEST_RUNNER 显式地设置, 这都是一个有争议的问题 — 如果这是显
    # 式地设置, 用户选择一组特定的行为, 而作为默认的变化不会改变.
    if not settings.is_overridden('TEST_RUNNER'):
        # Strong markers:
        # SITE_ID = 1 is in 1.5 template, not defined in 1.6 template.
        # KY: BASE_DIR 1.5模板中定义, 在1.6模板不再设置了.
        try:
            settings.SITE_ID
            weight += 2
        except AttributeError:
            pass

        # BASE_DIR is not defined in 1.5 template, set in 1.6 template.
        # KY: BASE_DIR 1.5模板中没定义, 在1.6模板设置了.
        try:
            settings.BASE_DIR
        except AttributeError:
            weight += 2

        # TEMPLATE_LOADERS defined in 1.5 template, not defined in 1.6 template.
        # KY: 在1.5中模板定义的TEMPLATE_LOADERS, 而不是1.6中定义的模板.
        if settings.is_overridden('TEMPLATE_LOADERS'):
            weight += 2

        # MANAGERS defined in 1.5 template, not defined in 1.6 template.
        # KY: 在1.5中模板定义的MANAGERS, 而不是1.6中定义的模板.
        if settings.is_overridden('MANAGERS'):
            weight += 2

        # Weaker markers - These are more likely to have been added in common usage
        # ADMINS defined in 1.5 template, not defined in 1.6 template.

        # KY: 较弱的标记 — 这些更有可能已经被添加, 在1.5中模板中以常见使用地定义ADMINS, 而不是1.6中定义的模板.
        if settings.is_overridden('ADMINS'):
            weight += 1

        # Clickjacking enabled by default in 1.6.
        # KY: Clickjacking 在1.6默认启用.
        if 'django.middleware.clickjacking.XFrameOptionsMiddleware' not in set(settings.MIDDLEWARE_CLASSES):
            weight += 1

    if weight >= 6:
        return [
            Warning(
                "Some project unittests may not execute as expected.",
                hint=("Django 1.6 introduced a new default test runner. It looks like "
                      "this project was generated using Django 1.5 or earlier. You should "
                      "ensure your tests are all running & behaving as expected. See "
                      "https://docs.djangoproject.com/en/dev/releases/1.6/#new-test-runner "
                      "for more information."),
                obj=None,
                id='1_6.W001',
            )
        ]
    else:
        return []


def _check_boolean_field_default_value(app_configs=None, **kwargs):
    """
    Checks if there are any BooleanFields without a default value, &
    warns the user that the default has changed from False to None.

    # KY: 检查是否有任何没有默认值的 BooleanFields, 警告用户默认值已经从False改到None.
    """

    from django.db import models

    problem_fields = [
        field
        for model in apps.get_models(**kwargs)
        if app_configs is None or model._meta.app_config in app_configs
        for field in model._meta.local_fields
        if isinstance(field, models.BooleanField) and not field.has_default()
    ]

    return [
        Warning(
            "BooleanField does not have a default value.",
            hint=("Django 1.6 changed the default value of BooleanField from False to None. "
                  "See https://docs.djangoproject.com/en/1.6/ref/models/fields/#booleanfield "
                  "for more information."),
            obj=field,
            id='1_6.W002',
        )
        for field in problem_fields
    ]
