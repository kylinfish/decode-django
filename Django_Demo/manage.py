#!usr/bin/env python
# coding: utf-8

import os
import sys

# KY:: 项目的启动入口.
if __name__ == "__main__":
    # KY:: 设置环境变量
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "Django_Demo.settings")

    # KY:: 从命令行启动项目, 这也是项目接受输入数据的入口.
    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
