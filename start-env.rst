Create VirtualEnv:
------------------
virtualenv Django-Env


Install Django:
---------------
ln -s [your-path]/decode-django/Django-1.7.5/django/ ./Django-Env/lib/python2.7/site-packages/django


Start Python VirtualEnv:
------------------------
source ./Django-Env/bin/activate
pip install -r requirements.txt


Create Demo Project:
--------------------
django-admin.py startproject Django_Demo

Change File Mode:
-----------------
sudo chmod -R 755 ../
