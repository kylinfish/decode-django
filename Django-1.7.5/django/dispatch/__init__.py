#!usr/bin/env python
# coding: utf-8

"""Multi-consumer multi-producer dispatching mechanism

Originally based on pydispatch (BSD) http://pypi.python.org/pypi/PyDispatcher/2.0.1
See license.txt for original license.

Heavily modified for Django's purposes.
"""

# KY: 多消费者 多生产者调度机制. 最初基于pydispatch.

from django.dispatch.dispatcher import Signal, receiver  # NOQA
